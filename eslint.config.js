// @ts-check

import eslint from '@eslint/js';
import tseslint from 'typescript-eslint';
import nodePlugin from 'eslint-plugin-n';
import securityPlugin from 'eslint-plugin-security';
import prettier from 'eslint-config-prettier';

export default tseslint.config(
    eslint.configs.recommended,
    ...tseslint.configs.strictTypeChecked,
    ...tseslint.configs.stylisticTypeChecked,
    nodePlugin.configs['flat/recommended-module'],
    securityPlugin.configs.recommended,
    prettier,
    {
        files: ['**/*.ts'],
    },
    {
        languageOptions: {
            ecmaVersion: 2022,
            sourceType: 'module',
            parserOptions: {
                project: true,
                tsconfigRootDir: import.meta.dirname,
            },
        },
    },
    {
        ignores: ['**/*.js'],
    },
    {
        rules: {
            quotes: ['error', 'single', { avoidEscape: true }],
            'handle-callback-err': 'off',
            'max-nested-callbacks': [
                'error',
                {
                    max: 4,
                },
            ],
            'max-statements-per-line': [
                'error',
                {
                    max: 2,
                },
            ],
            'no-console': 'off',
            'no-empty-function': [
                'error',
                {
                    allow: ['constructors'],
                },
            ],
            'no-lonely-if': 'error',
            'no-var': 'error',
            'prefer-const': 'error',
            'spaced-comment': 'error',
            yoda: 'error',
            'no-shadow': 'off',
            'n/no-missing-import': [
                'error',
                {
                    // I think these break because they end in .js
                    allowModules: ['discord.js', 'alexflipnote.js'],
                },
            ],
            '@typescript-eslint/restrict-template-expressions': [
                'error',
                {
                    allowNumber: true,
                },
            ],
            '@typescript-eslint/no-shadow': 'error',
            '@typescript-eslint/no-inferrable-types': [
                'error',
                {
                    ignoreParameters: true,
                },
            ],
        },
    },
);
