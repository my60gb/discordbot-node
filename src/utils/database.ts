import { PrismaClient, Quote } from '@prisma/client';

/**
 * Creates a quote and adds it to a user's list of quotes.
 * Creates the user first if they don't already exist in the db
 *
 */
export async function createQuote(
    prisma: PrismaClient,
    {
        text,
        quotedUser,
        quotedUserId,
        guildId,
        quotedByUser,
        quotedByUserId,
    }: {
        text: string;
        quotedUser: string;
        quotedUserId: number;
        guildId: number;
        quotedByUser: string;
        quotedByUserId: number;
    },
) {
    await prisma.user.upsert({
        create: {
            id: quotedByUserId,
            name: quotedByUser,
        },
        update: {},
        where: {
            id: quotedByUserId,
        },
    });

    await prisma.user.update({
        where: { id: quotedByUserId },
        data: { quotes: { create: { text, quotedUser, quotedUserId, guildId } } },
    });
}

/**
 * Get one quote by `quoteId`
 */
export async function getQuote(prisma: PrismaClient, quoteId: number): Promise<Quote | null> {
    return await prisma.quote.findUnique({ where: { id: quoteId } });
}

/**
 * Deletes a quote by `quoteId`
 */
export async function deleteQuote(prisma: PrismaClient, quoteId: number) {
    await prisma.quote.delete({ where: { id: quoteId } });
}

/**
 * Deletes a user and all their saved quotes
 */
export async function deleteUser(prisma: PrismaClient, userId: number) {
    await prisma.quote.deleteMany({ where: { quotedByUserId: userId } });
    await prisma.user.delete({ where: { id: userId } });
}

/**
 * Get all quotes saved by a user by `userId`
 */
export async function getUserSavedQuotes(
    prisma: PrismaClient,
    userId: number,
): Promise<Quote[] | null> {
    return await prisma.user
        .findUnique({ where: { id: userId }, select: { quotes: true } })
        .quotes();
}

/**
 * Get all quotes written by a user by `quotedUserId`
 */
export async function getQuotesByUser(
    prisma: PrismaClient,
    quotedUserId: number,
): Promise<Quote[] | null> {
    return await prisma.quote.findMany({ where: { quotedUserId } });
}
