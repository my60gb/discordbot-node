import { InteractionReplyOptions, MessageFlags } from 'discord.js';
import { setTimeout } from 'timers/promises';
import { CustomInteraction } from '../interaction.js';

/**
 * Pre-made error strings
 *
 */
export enum Errors {
    DifferentChannel = 'You must be in the same voice channel as the bot',
    NotInVoiceChannel = 'You must be in a voice channel',
    UnknownError = 'Something went wrong',
    NothingPlaying = 'Nothing is playing',
    EmptyQueue = 'The queue is empty',
    PausedQueue = 'The queue is paused',
    InvalidQueueIndex = 'Invalid queue index',
}

export function createErrorMessage(response: Errors): InteractionReplyOptions {
    return { content: response, flags: MessageFlags.Ephemeral };
}

export const ErrorMessage = {
    DifferentChannel: createErrorMessage(Errors.DifferentChannel),
    NotInVoiceChannel: createErrorMessage(Errors.NotInVoiceChannel),
    UnknownError: createErrorMessage(Errors.UnknownError),
    NothingPlaying: createErrorMessage(Errors.NothingPlaying),
    EmptyQueue: createErrorMessage(Errors.EmptyQueue),
    PausedQueue: createErrorMessage(Errors.PausedQueue),
    InvalidQueueIndex: createErrorMessage(Errors.InvalidQueueIndex),
};

export async function deleteReply(
    interaction: CustomInteraction,
    delay?: number,
): Promise<boolean> {
    if (delay && !Number.isFinite(delay)) {
        console.error('Delay must be a number');
        return false;
    }

    if (interaction.replied) {
        await setTimeout(delay);
        await interaction.deleteReply();

        return true;
    } else {
        console.error("Interaction wasn't replied to so no reply to delete");
        return false;
    }
}
