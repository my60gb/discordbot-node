import { AlexError, FileResponse } from 'alexflipnote.js';

export type ImageType = 'birb' | 'cats' | 'dogs' | 'sadcat' | 'coffee';

/**
 * Type guard for AlexError
 *
 * @param response Response received from the api
 */
export function isAlexError(response: FileResponse | AlexError): response is AlexError {
    return Object.keys(response).includes('code');
}
