/**
 * Gets a random number
 *
 * @param min Defaults to 0
 * @param decimalPlaces Defaults to 0. Must be >= 0
 */
export function getRandomNum(max: number, min: number = 0, decimalPlaces: number = 0): number {
    if (min > max) {
        throw new Error('min must be less than or equal to max');
    }
    if (decimalPlaces < 0) {
        throw new Error('decimalPlaces must be greater than or equal to 0');
    }

    if (decimalPlaces === 0) {
        return getRandomInt(max, min);
    } else {
        return Number.parseFloat(getRandomFloat(max, min).toFixed(decimalPlaces));
    }
}

/**
 * Gets a random integer
 *
 * @param {number}max Maximum value
 * @param {number}[min=0] Optional minimum value. Default is 0.
 * @returns {number} An integer between min and max inclusive
 */
function getRandomInt(max: number, min: number = 0): number {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1) + min);
}

/**
 * Gets a random float
 *
 * @param {number}max Maximum value
 * @param {number}[min=0] Optional minimum value. Default is 0.
 * @returns {number} A number between min and max inclusive
 */
function getRandomFloat(max: number, min: number = 0): number {
    return Math.random() * (max - min + 1) + min;
}

/**
 * Gets a random index of an array
 *
 * @param {number}length Length of the array
 * @returns {number}An integer from 0 to (length - 1)
 */
export function getRandomIndex(length: number): number {
    if (!Number.isInteger(length)) {
        throw new Error('length must be an integer');
    }

    return getRandomNum(length - 1);
}

/**
 * Returns a random item from an array
 *
 */
export function getRandomItem<T>(array: T[]): T {
    if (array.length === 0) {
        throw new Error('array is empty');
    }
    const index = getRandomIndex(array.length);
    const item = array.at(index);
    if (item === undefined) {
        throw new Error('something went very wrong with an array');
    } else {
        return item;
    }
}

/**
 * Check that the index is contained in the array
 * @template T
 * @param {Array<T>} array
 * @param {number} index
 * @returns {boolean} True if the index is valid, false if not
 */
export function isValidIndex<T>(array: T[], index: number): boolean {
    return array.at(index) !== undefined;
}

/**
 * Checks if a number is in a range of numbers
 */
export function inRange(num: number, min: number, max: number): boolean {
    return num >= min && num <= max;
}
