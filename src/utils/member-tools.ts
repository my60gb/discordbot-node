import {
    ChannelType,
    ChatInputCommandInteraction,
    CommandInteraction,
    Guild,
    GuildMember,
    VoiceBasedChannel,
} from 'discord.js';
import { Queue } from 'distube';

/**
 *
 * @param {CommandInteraction} interaction
 * @returns {boolean} true if interaction is a ChatInputCommandInteraction
 */
export function isChatInputCommandInteraction(
    interaction: CommandInteraction,
): interaction is ChatInputCommandInteraction {
    return interaction instanceof ChatInputCommandInteraction;
}

/**
 *
 */
export function isGuildMember(member: unknown): member is GuildMember {
    return member instanceof GuildMember;
}

/**
 *
 * @param {unknown} guild
 * @returns {boolean} true if the guild is a valid guild
 */
export function isGuild(guild: unknown): guild is Guild {
    return guild instanceof Guild;
}

/**
 * Checks if member is in a voice channel
 *
 * @param {CommandInteraction} interaction
 * @returns {boolean} true if member is in a voice channel, false otherwise
 */
export function inVoiceChannel(interaction: CommandInteraction): boolean {
    if (!isGuildMember(interaction.member)) {
        return false;
    }

    return interaction.member.voice.channel?.type === ChannelType.GuildVoice;
}

/**
 * Checks if member is in the voice channel the bot is in
 *
 * @param {Queue} queue Queue of the bot to check
 * @returns {boolean} true if member is in the same channel as the bot, false otherwise
 */
export function inSameVoiceChannel(interaction: CommandInteraction, queue?: Queue): boolean {
    if (!isGuildMember(interaction.member)) {
        return false;
    }
    if (queue?.voiceChannel == null) {
        return false;
    }
    return interaction.member.voice.channel === queue.voiceChannel;
}

/**
 * Returns the voice channel a member is in.
 * Returns undefined if they are not in a voice channel
 *
 * @param {CommandInteraction}interaction
 * @returns {VoiceBasedChannel | undefined} A voice channel or undefined
 */
export function getVoiceChannel(interaction: CommandInteraction): VoiceBasedChannel | undefined {
    if (inVoiceChannel(interaction)) {
        const member = interaction.member;
        if (!isGuildMember(member)) {
            return undefined;
        }
        return member.voice.channel ?? undefined;
    } else {
        return undefined;
    }
}
