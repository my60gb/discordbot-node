import { hideLinkEmbed } from 'discord.js';
import { SearchResultType, Song } from 'distube';
import { CustomClient } from '../client.js';

/**
 * Format song info to just the name as a link
 *
 * @param {Song} song
 * @returns {string} The song name as a link
 */
export function formatSongNameWithLink(song: Song): string {
    const songName = getSongName(song);

    return `[${songName}](${hideLinkEmbed(song.url)})`;
}

/**
 * Take an array of songs and format them as a readable list
 *
 * @param {Song[]} songArray
 * @param {number} page
 * @returns {string} Formatted list of songs
 */
export function formatSongList(songArray: Song[], page: number = 1, songsPerPage: number): string {
    const PageMultiplier = (page - 1) * songsPerPage;

    const formattedString = songArray
        .map((song, id) => {
            return `**${id + 1 + PageMultiplier}**. [${song.name ?? 'Unknown'}](${song.url}) - \`${
                song.formattedDuration ?? ''
            }\``;
        })
        .join('\n');
    return formattedString;
}

/**
 *
 * @param {string} searchTerms Search terms for youtube
 * @param {CustomClient} client
 * @returns {Promise<string | undefined>} A youtube url as a string or undefined if no results are found
 */
export async function getVideoUrl(
    searchTerms: string,
    client: CustomClient,
): Promise<string | undefined> {
    const distube = client.distube;
    const searchResult = await distube.search(searchTerms, {
        type: SearchResultType.VIDEO,
    });
    return searchResult[0].url;
}

/**
 *
 * @param {Song}song
 * @returns {string} The song name
 */
export function getSongName(song: Song): string {
    return song.name ?? 'Unknown song';
}
