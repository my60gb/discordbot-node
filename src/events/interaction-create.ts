import { Events, Interaction } from 'discord.js';
import { isCustomInteraction } from '../interaction.js';

export const interactionCreate = {
    name: Events.InteractionCreate,

    once: false,

    /**
     * Runs when any interaction occurs
     *
     * @param interaction Interaction that triggered the event
     */
    async execute(interaction: Interaction) {
        if (interaction.isChatInputCommand() || interaction.isContextMenuCommand()) {
            try {
                if (!isCustomInteraction(interaction)) {
                    throw new Error('Interaction is not a CustomInteraction');
                }
                const command = interaction.client.getCommand(interaction.commandName);
                await command.execute(interaction);
            } catch (error) {
                console.error(error);
            }
        }
    },
};
