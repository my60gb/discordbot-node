import { interactionCreate } from './events/interaction-create.js';
import { ready } from './events/ready.js';

/**
 * List of events
 */
const events = [interactionCreate, ready];

export { events, ready, interactionCreate };
