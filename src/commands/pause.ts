import { SlashCommandBuilder } from 'discord.js';
import { CustomInteraction } from '../interaction.js';
import { inSameVoiceChannel } from '../utils/member-tools.js';
import { ErrorMessage } from '../utils/reply.js';
import { SLASH_COMMAND } from '../models/core/command-type.js';

export const name = 'pause';
export const description = 'Pause the current song';
export const type = SLASH_COMMAND;

export const slashCommandData = new SlashCommandBuilder().setName(name).setDescription(description);

/**
 * Pauses the currently playing song
 *
 */
export async function execute(interaction: CustomInteraction) {
    const distube = interaction.client.distube;

    const queue = distube.getQueue(interaction);

    if (!inSameVoiceChannel(interaction, queue)) {
        await interaction.reply(ErrorMessage.DifferentChannel);
        return;
    }

    if (queue === undefined) {
        await interaction.reply(ErrorMessage.EmptyQueue);
        return;
    }

    if (queue.playing) {
        try {
            distube.pause(interaction);
        } catch (error) {
            console.error(error);
            await interaction.reply(ErrorMessage.UnknownError);

            return;
        }
        await interaction.reply('Paused');
    } else {
        await interaction.reply(ErrorMessage.NothingPlaying);
    }
}
