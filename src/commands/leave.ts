import { SlashCommandBuilder } from 'discord.js';
import { execute } from './stop.js';
import { SLASH_COMMAND } from '../models/core/command-type.js';

/**
 * Just an alias for the stop command
 */

export const name = 'leave';
export const description = 'Makes the bot leave the channel';
export const type = SLASH_COMMAND;

export const slashCommandData = new SlashCommandBuilder().setName(name).setDescription(description);

export { execute };
