import { SlashCommandBuilder } from 'discord.js';
import { CustomInteraction } from '../interaction.js';
import { SLASH_COMMAND } from '../models/core/command-type.js';

export const name = 'user';
export const description = 'Replies with user info';
export const type = SLASH_COMMAND;

export const slashCommandData = new SlashCommandBuilder().setName(name).setDescription(description);

/**
 * Replies with information about the user
 *
 */
export async function execute(interaction: CustomInteraction) {
    const message = `Your tag: ${interaction.user.tag}\nYour id: ${interaction.user.id}`;

    await interaction.reply(message);
}
