import { CommandInteraction, MessageFlags, SlashCommandBuilder } from 'discord.js';
import { Queue as DisTubeQueue, Song } from 'distube';
import { CustomInteraction } from '../interaction.js';
import { isValidIndex } from '../utils/general-tools.js';
import { inVoiceChannel } from '../utils/member-tools.js';
import { ErrorMessage } from '../utils/reply.js';
import { formatSongList } from '../utils/song-tools.js';
import { SLASH_COMMAND } from '../models/core/command-type.js';

export const SONGS_PER_PAGE = 10;

export const name = 'queue';
export const description = 'Shows the song queue';
export const type = SLASH_COMMAND;

export const slashCommandData = new SlashCommandBuilder()
    .setName(name)
    .setDescription(description)
    .addIntegerOption((option) =>
        option.setName('page').setDescription('Page of the queue to view').setRequired(false),
    );

export async function execute(interaction: CustomInteraction) {
    const queue = interaction.client.distube.getQueue(interaction);

    if (!inVoiceChannel(interaction)) {
        await interaction.reply(ErrorMessage.NotInVoiceChannel);

        return;
    }

    if (queue === undefined) {
        await interaction.reply(ErrorMessage.EmptyQueue);

        return;
    }

    const pageArray = paginateQueue(queue, SONGS_PER_PAGE);
    const numPages = pageArray.length;

    /** Page defaults to 1 */
    const page = interaction.options.getInteger('page') ?? 1;

    if (!isValidIndex(pageArray, page - 1)) {
        await interaction.reply(ErrorMessage.InvalidQueueIndex);

        return;
    }

    await displayQueue(interaction, pageArray, page);

    // Shows the page count
    if (numPages > 1) {
        await interaction.followUp(`Page ${page}/${numPages}`);
    }
}

/**
 * Takes a queue and splits it into an array of 10 song arrays
 *
 * @returns An array of up to 10 song arrays
 */
function paginateQueue(queue: DisTubeQueue, songsPerPage: number): Song[][] {
    const pages = Math.ceil(queue.songs.length / songsPerPage);
    const songArrays: Song[][] = [];
    let startIndex = 0;

    for (let page = 1; page <= pages; page++) {
        songArrays.push(queue.songs.slice(startIndex, songsPerPage + startIndex));
        startIndex = startIndex + songsPerPage;
    }

    return songArrays;
}

/**
 * Replies with list of songs
 *
 * @param pageArray
 * @param page Queue page to view
 */
async function displayQueue(
    interaction: CommandInteraction,
    pageArray: Song[][],
    page: number,
): Promise<void> {
    const songArray = pageArray.slice(page - 1, page)[0];
    const queueString = formatSongList(songArray, page, SONGS_PER_PAGE);

    await interaction.reply({
        content: queueString,
        flags: MessageFlags.SuppressEmbeds,
    });
}
