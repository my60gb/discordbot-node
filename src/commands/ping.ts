import { SlashCommandBuilder } from 'discord.js';
import { CustomInteraction } from '../interaction.js';
import { SLASH_COMMAND } from '../models/core/command-type.js';

export const name = 'ping';
export const description = 'Replies with Pong!';
export const type = SLASH_COMMAND;

export const slashCommandData = new SlashCommandBuilder().setName(name).setDescription(description);

export async function execute(interaction: CustomInteraction) {
    const sentMessage = await interaction.reply({
        content: 'Pinging...',
        fetchReply: true,
    });

    await interaction.editReply(
        `:ping_pong: Pong! \n:sparkling_heart: Websocket heartbeat: ${
            interaction.client.ws.ping
        }ms.\n:round_pushpin: Roundtrip Latency: ${
            sentMessage.createdTimestamp - interaction.createdTimestamp
        }ms`,
    );
}
