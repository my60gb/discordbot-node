import { MessageFlags, SlashCommandBuilder } from 'discord.js';
import { CustomInteraction } from '../interaction.js';
import { inSameVoiceChannel } from '../utils/member-tools.js';
import { ErrorMessage } from '../utils/reply.js';
import { SLASH_COMMAND } from '../models/core/command-type.js';

export const name = 'resume';
export const description = 'Resume playing';
export const type = SLASH_COMMAND;

export const slashCommandData = new SlashCommandBuilder().setName(name).setDescription(description);

/**
 * Resumes playing the first song in the queue
 *
 */
export async function execute(interaction: CustomInteraction) {
    const distube = interaction.client.distube;
    const queue = distube.getQueue(interaction);

    if (!inSameVoiceChannel(interaction, queue)) {
        await interaction.reply(ErrorMessage.DifferentChannel);

        return;
    }

    if (queue === undefined) {
        await interaction.reply(ErrorMessage.EmptyQueue);

        return;
    }

    if (queue.paused) {
        try {
            distube.resume(interaction);
        } catch (error) {
            console.error(error);
            await interaction.reply(ErrorMessage.UnknownError);
        }
        await interaction.reply('Resumed');
    } else if (queue.playing) {
        await interaction.reply({
            content: "It's already playing ya dope",
            flags: MessageFlags.Ephemeral,
        });
    } else {
        await interaction.reply({
            content: "It ain't paused",
            flags: MessageFlags.Ephemeral,
        });
    }
}
