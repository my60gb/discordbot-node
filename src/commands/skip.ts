import { MessageFlags, SlashCommandBuilder } from 'discord.js';
import { CustomInteraction } from '../interaction.js';
import { inVoiceChannel } from '../utils/member-tools.js';
import { ErrorMessage } from '../utils/reply.js';
import { formatSongNameWithLink } from '../utils/song-tools.js';
import * as stop from './stop.js';
import { SLASH_COMMAND } from '../models/core/command-type.js';

export const name = 'skip';
export const description = 'Skips to the next song in the queue';
export const type = SLASH_COMMAND;

export const slashCommandData = new SlashCommandBuilder().setName(name).setDescription(description);

/**
 * Skips the song currently playing
 *
 */
export async function execute(interaction: CustomInteraction) {
    if (!inVoiceChannel(interaction)) {
        await interaction.reply(ErrorMessage.NotInVoiceChannel);

        return;
    }

    const distube = interaction.client.distube;
    const queue = distube.getQueue(interaction);

    if (queue === undefined) {
        await interaction.reply({
            content: "Ain't no queue round these parts",
            flags: MessageFlags.Ephemeral,
        });

        return;
    }

    const song = formatSongNameWithLink(queue.songs[0]);

    /** If skipping the only song in the queue then stop */
    if (queue.songs.length === 1) {
        await stop.execute(interaction);
    } else {
        await distube.skip(interaction);
        await interaction.reply({
            content: `Skipping: ${song}`,
            flags: MessageFlags.SuppressEmbeds,
        });
    }
}
