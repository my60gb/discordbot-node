import { APIEmbedField, Colors, EmbedBuilder, SlashCommandBuilder } from 'discord.js';
import { BOT_NAME, CustomInteraction } from '../interaction.js';
import { SLASH_COMMAND } from '../models/core/command-type.js';
import { Command } from '../command.js';
import { getCommandCollection } from '../commands.js';
import { Collection } from '@discordjs/collection';

export const name = 'help';
export const description = 'Shows a list of available commands';
export const type = SLASH_COMMAND;

export const slashCommandData = new SlashCommandBuilder()
    .setName(name)
    .setDescription(description)
    .addStringOption((option) =>
        option.setName('command').setDescription('Specify a specific command').setRequired(false),
    );

/**
 * Fetches and replies with basic command information
 */
export async function execute(interaction: CustomInteraction) {
    const specifiedCommand = interaction.options.getString('command');
    const commands = (await getCommandCollection()).filter((command) => {
        if (specifiedCommand != undefined) {
            if (command.name === specifiedCommand) {
                return command;
            }
        } else {
            return command;
        }
    });

    if (commands.size > 0) {
        await interaction.reply({ embeds: [buildReply(commands)], ephemeral: true });
    } else {
        await interaction.reply({
            embeds: [buildCommandNotFoundError(specifiedCommand)],
            ephemeral: true,
        });
    }
}

function buildCommandNotFoundError(specifiedCommand: string | null) {
    return new EmbedBuilder()
        .setTitle(`${BOT_NAME} Help`)
        .setColor(Colors.Blurple)
        .addFields({ name: 'Error: ', value: `Command not found: "${specifiedCommand}"` });
}

function buildReply(commands: Collection<string, Command>) {
    return new EmbedBuilder()
        .setTitle(`${BOT_NAME} Help`)
        .setColor(Colors.Blurple)
        .addFields(buildFields(commands));
}

function buildFields(commands: Collection<string, Command>) {
    return commands.map((command) => {
        return {
            name: `/${command.name}`,
            value: command.description,
        } as APIEmbedField;
    });
}
