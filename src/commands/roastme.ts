import { MessageFlags, SlashCommandBuilder } from 'discord.js';
import { CustomInteraction } from '../interaction.js';
import { getInsult } from '../utils/insult-list.js';
import { SLASH_COMMAND } from '../models/core/command-type.js';

const ERROR_TEXT = 'Something went wrong. Like when you were born.';

export const name = 'roastme';
export const description = 'Roasts the hell outta you';
export const type = SLASH_COMMAND;

export const slashCommandData = new SlashCommandBuilder().setName(name).setDescription(description);

export async function execute(interaction: CustomInteraction) {
    try {
        await interaction.reply(getInsult());
    } catch (error) {
        console.error(error);
        await interaction.reply({
            content: ERROR_TEXT,
            flags: MessageFlags.Ephemeral,
        });
    }
}
