import { MessageFlags, SlashCommandBuilder } from 'discord.js';
import { CustomInteraction } from '../interaction.js';
import { isValidIndex } from '../utils/general-tools.js';
import { inVoiceChannel } from '../utils/member-tools.js';
import { ErrorMessage } from '../utils/reply.js';
import { formatSongNameWithLink } from '../utils/song-tools.js';
import * as skip from './skip.js';
import { SLASH_COMMAND } from '../models/core/command-type.js';

export const name = 'remove';
export const description = 'Remove a song from the queue';
export const type = SLASH_COMMAND;

export const slashCommandData = new SlashCommandBuilder()
    .setName(name)
    .setDescription(description)
    .addIntegerOption((option) =>
        option.setName('num').setDescription('Queue position to remove').setRequired(true),
    );

/**
 * Removes a song from the queue
 *
 */
export async function execute(interaction: CustomInteraction) {
    if (!inVoiceChannel(interaction)) {
        await interaction.reply(ErrorMessage.NotInVoiceChannel);

        return;
    }

    const distube = interaction.client.distube;
    const queue = distube.getQueue(interaction);

    if (queue === undefined) {
        await interaction.reply(ErrorMessage.UnknownError);

        return;
    }

    const songNumber = interaction.options.getInteger('num', true);
    const songIndex = songNumber - 1;

    if (!isValidIndex(queue.songs, songIndex)) {
        await interaction.reply(ErrorMessage.InvalidQueueIndex);

        return;
    }

    /** Skips if the removed song is currently playing */
    if (songIndex === 0) {
        await skip.execute(interaction);
        return;
    }

    const song = queue.songs.at(songIndex);

    if (song === undefined) {
        await interaction.reply({
            content: "Couldn't get song info",
            flags: MessageFlags.Ephemeral,
        });

        return;
    }

    await interaction.reply(`Removing ${formatSongNameWithLink(song)}`);
    queue.songs.splice(songIndex, 1);
}
