import { SlashCommandBuilder } from 'discord.js';
import { CustomInteraction } from '../interaction.js';
import { isAlexError } from '../utils/alex-api-call.js';
import { ErrorMessage } from '../utils/reply.js';
import { SLASH_COMMAND } from '../models/core/command-type.js';

export const name = 'birb';
export const description = 'Gets a random birb image';
export const type = SLASH_COMMAND;

export const slashCommandData = new SlashCommandBuilder().setName(name).setDescription(description);

/**
 * Replies with a url to a birb image from alexflipnote's api
 *
 */
export async function execute(interaction: CustomInteraction) {
    await interaction.deferReply();

    const response = await interaction.client.alexClient.birb();

    if (isAlexError(response)) {
        console.error(`Error code: ${response.code} \n${response.name}: ${response.description}`);
        await interaction.editReply(ErrorMessage.UnknownError);

        return;
    }

    await interaction.editReply(response.file);
}
