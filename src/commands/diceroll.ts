import { SlashCommandBuilder } from 'discord.js';
import { CustomInteraction } from '../interaction.js';
import { getRandomNum } from '../utils/general-tools.js';
import { SLASH_COMMAND } from '../models/core/command-type.js';

export const name = 'diceroll';
export const description = 'Number of sides for the die. 6 by default';
export const type = SLASH_COMMAND;

export const slashCommandData = new SlashCommandBuilder()
    .setName(name)
    .setDescription('Rolls a die')
    .addIntegerOption((option) =>
        option.setName('sides').setDescription(description).setRequired(false),
    );

/**
 * Replies with a random number from `1` to `sides`
 *
 * Defaults to 6 sides like normal dice
 *
 */
export async function execute(interaction: CustomInteraction) {
    const sides = interaction.options.getInteger('sides') ?? 6;

    await interaction.reply(`You rolled a ${getRandomNum(sides, 1)}`);
}
