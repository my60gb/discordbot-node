import { SlashCommandBuilder } from 'discord.js';
import { CustomInteraction } from '../interaction.js';
import { getRandomNum } from '../utils/general-tools.js';
import { SLASH_COMMAND } from '../models/core/command-type.js';

export const name = 'coinflip';
export const description = 'Flip a coin';
export const type = SLASH_COMMAND;

export const slashCommandData = new SlashCommandBuilder().setName(name).setDescription(description);

/**
 * Replies with either Heads or Tails with around a 50/50 chance
 *
 */
export async function execute(interaction: CustomInteraction) {
    await interaction.reply(getRandomNum(1) === 0 ? 'Heads' : 'Tails');
}
