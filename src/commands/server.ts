import { SlashCommandBuilder } from 'discord.js';
import { CustomInteraction } from '../interaction.js';
import { SLASH_COMMAND } from '../models/core/command-type.js';

export const name = 'server';
export const description = 'Replies with server info';
export const type = SLASH_COMMAND;

export const slashCommandData = new SlashCommandBuilder().setName(name).setDescription(description);

/**
 * Replies with server info
 *
 */
export async function execute(interaction: CustomInteraction) {
    const message = `Server name: ${interaction.guild?.name ?? 'Unknown'}\nTotal members: ${
        interaction.guild?.memberCount ?? 'Unknown'
    }`;

    await interaction.reply(message);
}
