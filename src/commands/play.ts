import { SlashCommandBuilder, VoiceBasedChannel } from 'discord.js';
import { DisTubeError } from 'distube';
import { CustomInteraction } from '../interaction.js';
import { getVoiceChannel } from '../utils/member-tools.js';
import { deleteReply, ErrorMessage } from '../utils/reply.js';
import { formatSongNameWithLink } from '../utils/song-tools.js';
import { SLASH_COMMAND } from '../models/core/command-type.js';
import { execute as resume } from './resume.js';

export const name = 'play';
export const description = 'Play a song or resume a paused song';
export const type = SLASH_COMMAND;

export const slashCommandData = new SlashCommandBuilder()
    .setName(name)
    .setDescription(description)
    .addStringOption((option) =>
        option.setName('search').setDescription('Song url or search terms').setRequired(false),
    );

/**
 * Plays music from a given url or search terms
 *
 * User must be in a voice channel for this to work
 *
 */
export async function execute(interaction: CustomInteraction) {
    const search = interaction.options.getString('search', false);
    const channel = getVoiceChannel(interaction);

    if (channel === undefined) {
        await interaction.reply(ErrorMessage.NotInVoiceChannel);
        return;
    }
    const queue = interaction.client.distube.getQueue(channel);

    if (search === null || search === '') {
        if (queue?.paused) {
            await resume(interaction);
        } else {
            await interaction.reply('No search terms provided');
        }
        return;
    }

    await interaction.deferReply();

    await play(interaction, search, channel);
}

/**
 * Plays song from url or search terms
 *
 * @param search Url or search terms to play from
 */
async function play(interaction: CustomInteraction, search: string, channel: VoiceBasedChannel) {
    const distube = interaction.client.distube;
    let message = '';

    try {
        await distube.play(channel, search);
    } catch (error) {
        await handlePlayerError(error, interaction);
        await deleteReply(interaction, 10000);

        return;
    }

    const queue = distube.getQueue(channel);
    if (queue === undefined) {
        console.error('Queue undefined for some reason...');
        await interaction.editReply(ErrorMessage.UnknownError);

        distube.voices.leave(channel);

        await deleteReply(interaction, 2000);
        return;
    }

    const lastSong = queue.songs.slice(-1)[0];

    // Most recently added song
    const lastSongName = formatSongNameWithLink(lastSong);

    // Song currently playing
    const songName = formatSongNameWithLink(queue.songs[0]);

    // Only write the song name if we've only queued/played one song
    const addedSongs = queue.songs.length - 1;

    if (addedSongs > 2) {
        message = `Added ${addedSongs} songs to queue`;
    } else {
        message =
            queue.songs.length === 1
                ? `Now playing: ${songName}`
                : `Added to queue: ${lastSongName}`;
    }

    await interaction.editReply(message);
}

async function handlePlayerError(error: unknown, interaction: CustomInteraction) {
    let message = '';

    if (error instanceof DisTubeError) {
        if (error.message.includes('HTTP Error 404')) {
            message = 'Error: 404 Not Found';
        } else if (error.message.includes('Unsupported URL')) {
            message = 'Error: Unsupported URL';
        } else if (error.errorCode == 'VOICE_FULL') {
            message = 'Error: Voice channel full';
        } else if (error.errorCode == 'VOICE_MISSING_PERMS') {
            message = 'Error: Missing voice channel permissions';
        } else if (error.errorCode == 'VOICE_CONNECT_FAILED') {
            message = "Error: Can't connect to voice channel";
        } else {
            console.error(error);

            message = "Error: Couldn't get song audio";
        }
    } else {
        console.error(error);

        message = "Couldn't play song";
    }

    await interaction.editReply(message);
}
