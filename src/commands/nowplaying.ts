import { MessageFlags, SlashCommandBuilder } from 'discord.js';
import { CustomInteraction } from '../interaction.js';
import { ErrorMessage } from '../utils/reply.js';
import { formatSongNameWithLink } from '../utils/song-tools.js';
import { SLASH_COMMAND } from '../models/core/command-type.js';

export const name = 'nowplaying';
export const description = 'Displays currently playing song';
export const type = SLASH_COMMAND;

export const slashCommandData = new SlashCommandBuilder().setName(name).setDescription(description);

/**
 * Replies with the song that is currently playing
 *
 */
export async function execute(interaction: CustomInteraction) {
    const queue = interaction.client.distube.getQueue(interaction);

    if (queue === undefined) {
        await interaction.reply(ErrorMessage.EmptyQueue);

        return;
    }

    if (queue.playing) {
        const currentSong = queue.songs[0];

        await interaction.reply({
            content: `Now playing: ${formatSongNameWithLink(currentSong)}`,
            flags: MessageFlags.SuppressEmbeds,
        });

        return;
    }

    await interaction.reply(ErrorMessage.NothingPlaying);
}
