import { SlashCommandBuilder } from 'discord.js';
import { CustomInteraction } from '../interaction.js';
import { inSameVoiceChannel, inVoiceChannel } from '../utils/member-tools.js';
import { deleteReply, ErrorMessage } from '../utils/reply.js';
import { SLASH_COMMAND } from '../models/core/command-type.js';

export const name = 'stop';
export const description = 'Makes the bot leave the voice channel';
export const type = SLASH_COMMAND;

export const slashCommandData = new SlashCommandBuilder().setName(name).setDescription(description);

/**
 * Stops playing and leaves the voice channel
 *
 */
export async function execute(interaction: CustomInteraction) {
    if (!inVoiceChannel(interaction)) {
        await interaction.reply(ErrorMessage.NotInVoiceChannel);

        return;
    }

    const distube = interaction.client.distube;
    const queue = distube.getQueue(interaction);

    if (queue === undefined) {
        await interaction.reply(ErrorMessage.EmptyQueue);

        return;
    }

    if (!inSameVoiceChannel(interaction, queue)) {
        await interaction.reply(ErrorMessage.DifferentChannel);

        return;
    }

    distube.voices.leave(interaction);
    await interaction.reply('Bye!');
    await deleteReply(interaction, 1000);
}
