import { ContextMenuCommandBuilder, SlashCommandBuilder } from 'discord.js';
import { CustomInteraction } from './interaction.js';
import { CONTEXT_MENU_COMMAND, SLASH_COMMAND } from './models/core/command-type.js';

export type SlashCommandData = Omit<SlashCommandBuilder, 'addSubcommand' | 'addSubcommandGroup'>;
export type ContextCommandData = Omit<
    ContextMenuCommandBuilder,
    'addSubcommand' | 'addSubcommandGroup'
>;

export interface Command {
    readonly name: string;
    readonly type: string;
    readonly description: string;
    readonly slashCommandData: SlashCommandData;
    readonly contextCommandData: ContextCommandData;
    execute(interaction: CustomInteraction): Promise<void>;
}

/**
 * Checks if the object has all the keys of a Command
 *
 * @param object The object to check
 */
export function isCommandObject(object: unknown): object is Command {
    try {
        const commandObject = object as Command;
        const keys = Object.keys(commandObject);
        const requiredKeys = commandTypeKeyMap.get(commandObject.type);

        if (requiredKeys) {
            for (const key of requiredKeys) {
                if (!keys.includes(key)) {
                    return false;
                }
            }
        } else {
            return false;
        }

        return true;
    } catch (e) {
        console.error(e);
        return false;
    }
}

const commandTypeKeyMap = new Map<string, string[]>([
    [SLASH_COMMAND, ['name', 'type', 'slashCommandData', 'execute']],
    [CONTEXT_MENU_COMMAND, ['name', 'type', 'contextCommandData', 'execute']],
]);
