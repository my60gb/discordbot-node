import { Collection, ReadonlyCollection } from '@discordjs/collection';
import fs from 'node:fs';
import path, { dirname } from 'node:path';
import { fileURLToPath, pathToFileURL } from 'node:url';
import { Command, isCommandObject } from './command.js';

const COMMANDS_FOLDER = 'commands';

/**
 * Get a collection of all commands with
 * the command names as keys
 *
 */
export async function getCommandCollection(): Promise<ReadonlyCollection<string, Command>> {
    const commands = await loadCommandsFromFiles();

    return new Collection(commands.map((command) => [command.name, command]));
}

/**
 * @throws If no valid commands are loaded
 */
async function loadCommandsFromFiles(): Promise<Command[]> {
    const commands: Command[] = [];
    const commandFilePaths = getCommandFilePaths();

    for (const filePath of commandFilePaths) {
        const commandFile: unknown = await import(filePath);

        if (isCommandObject(commandFile)) {
            commands.push(commandFile);
        } else {
            console.error(
                `[WARNING] The file at ${filePath.toString()} isn't a proper command file`,
            );
        }
    }

    if (commands.length < 1) {
        throw new Error('No commands loaded');
    }

    return commands;
}

function getCommandFilePaths() {
    const commandsPath = getCommandDirPath();
    const commandFileNames = getCommandFileNames(commandsPath);

    return commandFileNames.map((fileName) =>
        pathToFileURL(path.join(commandsPath, fileName)).toString(),
    );
}

function getCommandDirPath() {
    const __filename = fileURLToPath(import.meta.url);
    const __dirname = dirname(__filename);
    return path.join(__dirname, COMMANDS_FOLDER);
}

/**
 * Returns all file names with `.js` or `.ts` extensions in the `commandsPath`
 *
 * @param commandsPath
 */
function getCommandFileNames(commandsPath: string): string[] {
    // eslint-disable-next-line security/detect-non-literal-fs-filename
    return fs
        .readdirSync(commandsPath, { encoding: 'utf8', recursive: true })
        .filter((fileName) => fileName.endsWith('.js') || fileName.endsWith('.ts'));
}
