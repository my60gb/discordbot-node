import { ChatInputCommandInteraction, Client, Interaction } from 'discord.js';
import { CustomClient } from './client.js';

/** Interaction type that includes our `CustomClient` */
export interface CustomInteraction extends ChatInputCommandInteraction {
    client: Client<true> & CustomClient;
}

export function isCustomInteraction(
    interaction: Interaction | CustomInteraction,
): interaction is CustomInteraction & ChatInputCommandInteraction {
    return interaction.client.isCustomClient();
}

export const BOT_NAME = 'Cyndabot';
