import 'dotenv/config';
import { z } from 'zod';

const BotConfigSchema = z.object({
    Token: z.string(),
    ClientId: z.string(),
    GuildId: z.string().optional(),
});

/**
 * GuildId is only needed if you need  to deploy commands to only one server
 */
export type BotConfig = Readonly<z.infer<typeof BotConfigSchema>>;

/**
 * Loads the bot config from a .env file
 *
 */
export function loadConfig(): BotConfig {
    const e = process.env;

    const config = {
        Token: e.TOKEN,
        ClientId: e.CLIENTID,
        GuildId: e.GUILDID,
    };

    const result = BotConfigSchema.safeParse(config);

    if (!result.success) {
        throw new Error('TOKEN and CLIENTID are both required');
    } else {
        return result.data;
    }
}
