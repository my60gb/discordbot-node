import { REST } from '@discordjs/rest';
import { Routes } from 'discord-api-types/v10';
import { getCommandCollection } from './commands.js';
import { loadConfig } from './config.js';
import { CONTEXT_MENU_COMMAND } from './models/core/command-type.js';

/**
 * Sends data for the slash commands to Discord
 *
 * This only needs to be run if new commands are added or existing commands add or change options
 *
 * @throws {Error} If there's an error during the REST API calls
 */
void (async () => {
    const config = loadConfig();
    const rest = new REST({ version: '10' }).setToken(config.Token);
    const commandData = (await getCommandCollection()).map((command) => {
        if (command.type === CONTEXT_MENU_COMMAND) {
            return command.contextCommandData;
        } else {
            return command.slashCommandData;
        }
    });

    // Deploy to all servers if no guildId specified
    if (config.GuildId === undefined) {
        try {
            await rest.put(Routes.applicationCommands(config.ClientId), {
                body: commandData,
            });
        } catch (e) {
            console.error(e);
            throw new Error('Failed to register commands');
        }

        console.log('Successfully registered application commands to all servers.');
    } else {
        try {
            await rest.put(Routes.applicationGuildCommands(config.ClientId, config.GuildId), {
                body: commandData,
            });
        } catch (e) {
            console.error(e);
            throw new Error(`Failed to register commands to guild with id ${config.GuildId}`);
        }

        console.log(
            `Successfully registered application commands to guild with id ${config.GuildId}`,
        );
    }
})();
