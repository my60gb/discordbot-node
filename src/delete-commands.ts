import { REST } from '@discordjs/rest';
import { Routes } from 'discord-api-types/v10';
import { loadConfig } from './config.js';

void (async () => {
    const config = loadConfig();
    const rest = new REST({ version: '10' }).setToken(config.Token);

    if (config.GuildId === undefined) {
        // Delete all commands globally
        try {
            await rest.put(Routes.applicationCommands(config.ClientId), { body: [] });
        } catch (e) {
            console.error(e);
            throw new Error('Failed to delete commands globally');
        }

        console.log('Successfully deleted all commands globally');
    } else {
        try {
            await rest.put(Routes.applicationGuildCommands(config.ClientId, config.GuildId), {
                body: [],
            });
        } catch (e) {
            console.error(e);
            throw new Error(`Failed to delete commands for guild with id ${config.GuildId}`);
        }

        console.log(`Successfully deleted all commands for guild with id ${config.GuildId}`);
    }
})();
