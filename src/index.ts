import { CustomClient } from './client.js';
import { getCommandCollection } from './commands.js';
import { loadConfig } from './config.js';

void (async () => {
    const config = loadConfig();
    const commandCollection = await getCommandCollection();
    const client = new CustomClient(commandCollection);

    if ((await client.login(config.Token)) !== config.Token) {
        throw new Error('Login error');
    }
})();
