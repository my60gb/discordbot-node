import { ReadonlyCollection } from '@discordjs/collection';
import { YtDlpPlugin } from '@distube/yt-dlp';
import AlexClient from 'alexflipnote.js';
import { Client, Events, GatewayIntentBits } from 'discord.js';
import { DisTube } from 'distube';
import { Command } from './command.js';
import { interactionCreate, ready } from './events.js';
import { ImageType } from './utils/alex-api-call.js';
import { PrismaClient } from '@prisma/client';

/** Adds distube and commands properties to the Client class */
export class CustomClient extends Client<true> {
    /**
     * DisTube attached to bot client
     *
     */
    readonly distube = new DisTube(this, {
        searchSongs: 5,
        searchCooldown: 30,
        leaveOnEmpty: true,
        leaveOnFinish: true,
        leaveOnStop: false,
        plugins: [new YtDlpPlugin({ update: true })],
    });

    /** Client for the alexflipnote api */
    readonly alexClient: AlexClient;

    readonly databaseClient: PrismaClient;
    /**
     * Gets a command from the client object
     *
     * @throws If the command is not found
     */
    getCommand(commandName: string): Command {
        const command = this.commandCollection.get(commandName);

        if (command === undefined) {
            throw new Error(`${commandName} is not a valid command`);
        }

        return command;
    }

    /**
     * Gets a random image
     *
     */
    async getRandomImage(imageType: ImageType) {
        // I believe this is safe because we're not using user input and
        // Typescript should only allow a valid key as input to the function
        // eslint-disable-next-line security/detect-object-injection
        return await this.alexClient[imageType]();
    }

    constructor(private readonly commandCollection: ReadonlyCollection<string, Command>) {
        super({
            intents: [
                GatewayIntentBits.Guilds,
                GatewayIntentBits.GuildVoiceStates,
                GatewayIntentBits.GuildMessageReactions,
                GatewayIntentBits.GuildMessageTyping,
                GatewayIntentBits.DirectMessages,
                GatewayIntentBits.DirectMessageTyping,
            ],
        });

        this.databaseClient = new PrismaClient();
        this.alexClient = new AlexClient();

        this.distube.on('error', (channel, error) => {
            console.error(error, `Distube error in ${channel?.name ?? 'Unknown channel'}`);
        });

        this.once(Events.ClientReady, () => {
            ready.execute(this);
        });

        this.on(Events.InteractionCreate, async (interaction) => {
            await interactionCreate.execute(interaction);
        });
    }
}

declare module 'discord.js' {
    interface Client {
        isCustomClient(): boolean;
    }
}

Client.prototype.isCustomClient = function () {
    return Object.keys(this).includes('distube');
};
