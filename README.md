# A discord bot with basic functions for playing music

Very much a hobby project.

## Requires

- Node.js >= 18
- ffmpeg

## Docker

Or run it with docker:

`docker run --env-file .env registry.gitlab.com/my60gb/discordbot-node`

or with podman:

`podman run --env-file .env registry.gitlab.com/my60gb/discordbot-node`

## Env file

Copy the format in env.example for your .env file
