-- CreateTable
CREATE TABLE "User" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "name" TEXT NOT NULL
);

-- CreateTable
CREATE TABLE "Quote" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "text" TEXT NOT NULL,
    "quotedUser" TEXT NOT NULL,
    "quotedUserId" INTEGER NOT NULL,
    "quotedByUserId" INTEGER NOT NULL,
    "guildId" INTEGER NOT NULL,
    CONSTRAINT "Quote_quotedByUserId_fkey" FOREIGN KEY ("quotedByUserId") REFERENCES "User" ("id") ON DELETE RESTRICT ON UPDATE CASCADE
);

-- CreateIndex
CREATE UNIQUE INDEX "User_id_key" ON "User"("id");
