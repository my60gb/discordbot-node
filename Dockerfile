FROM docker.io/node:20-alpine3.20 AS builder
WORKDIR /build
COPY . /build
RUN npm install \
    && npm run build \
    && npm prune --omit=dev --ignore-scripts

FROM docker.io/node:20-alpine3.20
WORKDIR /app
RUN apk -U add --no-cache ffmpeg tini
ENV NODE_ENV=production
USER node
ENTRYPOINT ["/sbin/tini", "--"]
COPY --from=builder --chown=node:node /build /app
CMD ["node","dist/index.js"]
